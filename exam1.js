// Write a program that prints the numbers from 1 to 100, with each number on its own line. But for multiples of three print "Fun" instead of the number and for the multiples of five print "Bar". For numbers which are multiples of both three and five print "FunBar".


const get_remainder = (number, divisor) => {
    let i = number;

    while(i >= divisor) {
        i -= divisor;
    }

    return i;
}

// console.log(get_remainder(20, 5));


const print_numbers = (multiple_num1, value_num1, multiple_num2, value_num2) => {
    const max = 100;
    let num = 0;

    while(num < max) {
        num++;

        if ((get_remainder(num, multiple_num1) === 0) && get_remainder(num, multiple_num2) === 0) {
            console.log(value_num1+value_num2);
            continue;
        }

        if ((get_remainder(num, multiple_num1)) === 0) {
            console.log(value_num1);
            continue;
        }

        if (get_remainder(num, multiple_num2) === 0) {
            console.log(value_num2);
            continue;
        }

        console.log(num);
    }
}

print_numbers(
    3,
    "Cat",
    5,
    "Dog"
);
