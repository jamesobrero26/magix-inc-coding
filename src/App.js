import React, { Component } from 'react'

import UserList from './components/UserList/UserList';
import NewUser from './components/NewUser/NewUser';

import './App.css'

export class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: []
    }
  }

  componentDidMount() {
    // fetch("https://jsonplaceholder.typicode.com/users")
    //   .then(response => response.json())
    //   .then(data => this.setState({ users: data }))
  }

  addNewUserHandler = newUser => {
    console.log(newUser);

    this.setState((state) => {
      return {
        ...state,
        users: state.users.concat(newUser)
      }
    })
  };

  render() {
    const { users } = this.state;

    console.log(users);

    return (
      <div className="users">
        <h2>Users</h2>
        <NewUser onAddUser={this.addNewUserHandler} />
        <UserList users={users} />
      </div>
    )
  }
}

export default App
