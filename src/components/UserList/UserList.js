import React, { Component } from "react";

import "./UserList.css";

export class UserList extends Component {
  render() {
    return (
      <ul className="user-list">
        {this.props.users.map(user => {
            return <li key={user.id}>{user.name}</li>
        })}
      </ul>
    );
  }
}

export default UserList;
