import React, { Component } from "react";

import './NewUser.css'

export class NewUser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: ''
    };
  }

  addNewUserHandler = event => {
    event.preventDefault();
    const { name } = this.state;
    const newUser = {
      id: Math.random().toString(),
      name
    }

    this.props.onAddUser(newUser);
    this.setState({ name: '' });
  }

  onTextChangeHandler = event => {
    this.setState({ name: event.target.value });
  }


  render() {
    const { name } = this.state;

    return (
        <form className="new-user" onSubmit={this.addNewUserHandler}>
            <input type="text" value={name} onChange={this.onTextChangeHandler} />
            <button type="submit">Add User</button>
        </form>
    );
  }
}

export default NewUser;
